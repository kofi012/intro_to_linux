# OS and Linux Intro class

This class will look into Os systems and mostly linux

### OS

Os stands for operating systems. They are a level of abstraction on the bare metal.

Some have GUI (graphical user interface) such as Android, Microsoft, MacOS and lots of linux distrubution.

Others do not.

Some are paid, others are open source.

We will be using Linux a lot.

### Linux

There are many linux distrubutions, here are some;
- ubuntu (user friendly, wildly used)
- Fedora
- Redhad distros
- Arch
- Kali (security testing and penetration testing/ Ethical hacking)

#### Bash and shells

Linux comes with Bash terminals, and they might have different shells.

Different shell behave differently.

- bash
- oh-my-zsh
- gitbash
- other

**small differences in some commands**

performed through the package manager

- RHEL = yum, dnf or rpm
- Debian = apt-get, aptitude or dpkg

These help install software - such as python or nginxand apace.

#### Aspects
Everything is a file
- everything can interact with everything - this is great for automation

#### 3 file_descriptors

To interact with programs ad with files. We can use, redirect and play around with 0, 1 and 2 that are the stdin, stdout, stderr

**Standrad input (stdin)**

**Standard output (stdout)**

**Standard error (stderr)**

### Two important hard paths `/` and `~`

`/` means root directory

`~` means `/user/your_login_user/`

### Environment variables

Environment is a place where code runs.
In bash we might want to set some variables.

Variable is like a box, you have a name and you add stuff to it. You can open it later.

In bash we define a variable simply by:

```bash

# check variable on terminal
$ ENV

# setting a variable
My_VAR="This is a VAR in my code"
>"This is a VAR in my code"

# call the variable using echo $
echo $MY_VAR

# You cab re assign the variable
MY_VAR=12345
echo $MY_VAR
> 12345

# Call other variables the same way
echo $USER
> kofi
```

What happens if i close the terminal and open a new one?

Once you close your terminal session, all variables not in the path will be lost.

#### Child process

A process that is initiated under another process, like running another bash script.

It goes via the $PATH but is a new "terminal"

Hence if we:

```bash
# 1) set variable in terminal
LANDLORD="Jess"

# run the bash script that has echo $LANDLORD
./bash_file.sh
> hi from the file
> 
> Landlord above^^^

# This happens because the ./bash_file.sh runs as a child process - A new terminal

# also you didnt export
export Landlord="Jess"
./bash_file.sh
>hi from the file
>
> Landlord above^^^


```

#### Setting & Defining Environment variables

Terminal & bash processes follow the PATH

There is a PATH for login users that have a profile and the ones without.

Files to consider:
```
> .zprofile
> .zshrc
```
### Path

Terminal & bash process follow the PATH

There is a PATH for login user that have a profile and the ones without.

```bash

```
### Common commands


## Wild cards

### Matchers

You can use this to help you match files or content files.

```
# any number of character to a side
ls exam*
> example     example.jpg example.txt example2

ls *exam
>this_is_an_exam

# ? for specific number of characters
> ls example????
>example.jpg example.txt

## List of specific characters
# each one of these represents one character
ls example.[aeiou][a-z][a-z]
```

### Redirects

Every command has 3 defaults.
- stdin - Represented by 0
- stdout - Represented by 1
- stderr - Represented by 2

example of stdout
```
ls
> README.md       example         example.txt     notes (...)

## the list that prints is the stdout!

```
example giving `ls` a stdin

```bash
# the example???? is stdin
ls example????
> example.jpg example.txt
# the output is stdout
```

If there is an error, you get stderr:
```bash
ls example???
>zsh: no matches found: example???
# the above is a stderr
```

The cool thing is you can redirect this!

##### > and >>

This `>` will redirect and truncate

This `>>` will redirect and append.

Use it with number to decide what to redirect.

#### messing with STDIN

`<`

### Pipes

Piping redirect and makes it stdin of another program

### Grep

Grep is a command used to search for text and strigns in a given file.

## Using Piping and Grep

### Piping and Grep example

```bash

# Using ls -l list all files in current directory with added info
ls -l
> -rw-r--r--  1 Kofi  staff  4034 13 Jan 17:10 README.md
-rwxr-xr-x  1 Kofi  staff   101 13 Jan 17:10 bash_file.sh
-rw-r--r--  1 Kofi  staff     0 13 Jan 16:46 example
-rw-r--r--  1 Kofi  staff     0 13 Jan 16:46 example.jpg
-rw-r--r--  1 Kofi  staff     0 13 Jan 16:46 example.txt
-rw-r--r--  1 Kofi  staff     0 13 Jan 16:46 example2
-rw-r--r--  1 Kofi  staff     0 13 Jan 14:05 notes
-rw-r--r--  1 Kofi  staff    54 13 Jan 14:28 random.txt
-rw-r--r--  1 Kofi  staff    24 13 Jan 17:04 stdout_file
-rw-r--r--  1 Kofi  staff     0 13 Jan 16:47 this_is_an_exam

# using piping and grep, we can filter the output to only search for files with the word "exam"
ls -l | grep exam
> -rw-r--r--  1 Kofi  staff     0 13 Jan 16:46 example
-rw-r--r--  1 Kofi  staff     0 13 Jan 16:46 example.jpg
-rw-r--r--  1 Kofi  staff     0 13 Jan 16:46 example.txt
-rw-r--r--  1 Kofi  staff     0 13 Jan 16:46 example2
-rw-r--r--  1 Kofi  staff     0 13 Jan 16:47 this_is_an_exam

```

## using `>` and `>>`

```bash

# using > destroys everything in the file and replaces it with the desired info
echo hello > VS_example.txt
cat VS_example.txt
>hello

#using `>>` adds the info into the info already on the file
echo hello world >> VS_example.txt
cat VS_example.txt
>hello
>hello world

```

## Redirect 